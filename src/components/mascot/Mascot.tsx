import * as React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import "./Mascot.scss"

const Mascot = () => {
  const data = useStaticQuery(graphql`
    query {
      placeholderImage: file(relativePath: { eq: "jojo.png" }) {
        childImageSharp {
          fluid(maxWidth: 1400) {
            ...GatsbyImageSharpFluid_noBase64
          }
        }
      }
    }
  `)

  return (
    <div className="container mascot" data-aos="fade-up">
      <Img
        fadeIn
        fluid={data.placeholderImage.childImageSharp.fluid}
        style={{ maxWidth: "60%", margin: "0 auto" }}
      />
    </div>
  )
}

export default Mascot
