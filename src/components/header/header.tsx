import PropTypes from "prop-types"
import * as React from "react"
import Logo from "../Logo"
import NavBar from "../navbar/NavBar"
import "./header.scss"

const Header = ({ siteTitle, sticky }) => {
  return (
    <header className={`main-navbar ${sticky ? "sticky" : ""}`} id="main-nav">
      <div className="logo">
        <Logo />
      </div>
      <NavBar />
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
  sticky: PropTypes.bool,
}

Header.defaultProps = {
  siteTitle: ``,
  sticky: false,
}

export default Header
