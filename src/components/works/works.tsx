import React, { ReactElement } from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import "./works.scss"

interface Props {}

function Works({}: Props): ReactElement {
  const data = useStaticQuery(graphql`
    query {
      baekonjay: file(relativePath: { eq: "works/baekonjay.PNG" }) {
        childImageSharp {
          fixed(width: 350) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      loft: file(relativePath: { eq: "works/loft.PNG" }) {
        childImageSharp {
          fixed(width: 350) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      grandu: file(relativePath: { eq: "works/grandu.PNG" }) {
        childImageSharp {
          fixed(width: 350) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      bmwleasing: file(relativePath: { eq: "works/bmwleasing.PNG" }) {
        childImageSharp {
          fixed(width: 350) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      fineart: file(relativePath: { eq: "works/fineart.PNG" }) {
        childImageSharp {
          fixed(width: 350) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      fineart2: file(relativePath: { eq: "works/fineart2.PNG" }) {
        childImageSharp {
          fixed(width: 350) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      performingarts: file(relativePath: { eq: "works/performingarts.PNG" }) {
        childImageSharp {
          fixed(width: 350) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return (
    <section id="works" data-aos="fade-up">
      <h3>
        <span className="half-border">WORKS</span>
      </h3>
      <div className="works-gallery">
        <div className="work" data-aos="fade-up" data-aos-delay="100">
          <Img
            fadeIn
            fixed={data.baekonjay.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div className="work-name">BAEKON JAY TRACKING</div>
        </div>

        <div className="work" data-aos="fade-up" data-aos-delay="200">
          <Img
            fadeIn
            fixed={data.loft.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div className="work-name">LOFT BANGKOK</div>
        </div>
        <div className="work" data-aos="fade-up" data-aos-delay="300">
          <Img
            fadeIn
            fixed={data.grandu.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div className="work-name">GRAND UNITY</div>
        </div>
        <div className="work" data-aos="fade-up" data-aos-delay="100">
          <Img
            fadeIn
            fixed={data.bmwleasing.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div className="work-name">BMW LEASING</div>
        </div>
        <div className="work" data-aos="fade-up" data-aos-delay="200">
          <Img
            fadeIn
            fixed={data.fineart.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div className="work-name">FINE ARTS DEPARTMENT</div>
        </div>
        <div className="work" data-aos="fade-up" data-aos-delay="300">
          <Img
            fadeIn
            fixed={data.fineart2.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div className="work-name">FINE ARTS DEPARTMENT</div>
        </div>
        <div className="work" data-aos="fade-up" data-aos-delay="100">
          <Img
            fadeIn
            fixed={data.performingarts.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div className="work-name">OFFICE OF PERFORMING ARTS</div>
        </div>
      </div>
    </section>
  )
}

export default Works
