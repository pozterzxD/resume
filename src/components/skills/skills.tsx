import React, { ReactElement, useEffect } from "react"
import "./skills.scss"
import * as am4core from "@amcharts/amcharts4/core"
import am4themes_animated from "@amcharts/amcharts4/themes/animated"
import * as forceDirected from "@amcharts/amcharts4/plugins/forceDirected"

interface Props {}

const data = [
  {
    name: "Frontend",
    value: "60",
    color: "#f29023",
    children: [
      {
        name: "Expert",
        color: "#f6ab57",
        children: [
          {
            name: "JS/TS",
            children: [
              {
                name: "Nodejs",
              },
              {
                name: "Reactjs",
              },
              {
                name: "Nextjs",
              },
            ],
          },
          {
            name: "CSS",
            children: [
              {
                name: "SCSS",
              },
              {
                name: "Bootstrap",
              },
              {
                name: "Ant Design",
              },
              {
                name: "Bulma",
              },
            ],
          },
          {
            name: "HTML",
          },
        ],
      },
      {
        name: "Intermediate",
        color: "#f6ab57",
        children: [
          {
            name: "JS/TS",
            children: [
              {
                name: "Angularjs",
              },
              {
                name: "Vuejs",
              },
              {
                name: "GatsbyJS",
              },
            ],
          },
          {
            name: "Golang",
          },
        ],
      },
      {
        name: "Basic",
        value: "20",
        color: "#f6ab57",
        children: [
          {
            name: "PHP",
            children: [
              {
                name: "Laravel",
              },
              {
                name: "CodeIgniter",
              },
            ],
          },
          {
            name: "C++",
          },
          {
            name: "C",
          },
        ],
      },
    ],
  },
  {
    name: "Backend",
    value: "60",
    color: "#658290",
    children: [
      {
        name: "Expert",
        color: "#6d96a9",
        children: [
          {
            name: "Nestjs",
          },
        ],
      },
      {
        name: "Intermediate",
        color: "#6d96a9",
        children: [
          {
            name: "Expressjs",
          },
        ],
      },
      {
        name: "Basic",
        value: "20",
        color: "#6d96a9",
        children: [
          {
            name: "PHP",
            children: [
              {
                name: "Laravel",
              },
              {
                name: "CodeIgniter",
              },
            ],
          },
        ],
      },
    ],
  },
  {
    name: "Other",
    value: "60",
    color: "#aeaeae",
    children: [
      {
        name: "Expert",
        color: "#c2c2c2",
        children: [
          { name: "Nginx" },
          { name: "Apache" },
          { name: "Agile" },
          { name: "Firebase" },
          { name: "LINE Bot" },
          { name: "Socket.io" },
          { name: "MYSQL" },
          { name: "PostgreSQL" },
          { name: "MongoDB" },
        ],
      },
      {
        name: "Intermediate",
        color: "#c2c2c2",
        children: [{ name: "Docker" }, { name: "Spark AR Studio" }],
      },
    ],
  },
]

function Skills({}: Props): ReactElement {
  useEffect(() => {
    am4core.useTheme(am4themes_animated)
    let c = am4core.create("skill-tree", forceDirected.ForceDirectedTree)
    let series = c.series.push(new forceDirected.ForceDirectedSeries())

    series.dataFields.value = "value"
    series.dataFields.name = "name"
    series.dataFields.children = "children"
    series.dataFields.color = "color"
    series.dataFields.fixed = "fixed"
    series.nodes.template.label.text = "{name}"
    series.fontSize = 13
    series.minRadius = 50
    series.maxRadius = 70
    series.maxLevels = 2

    series.data = data
  }, [])

  return (
    <section id="skills" data-aos="fade-left">
      <h3>
        <span className="half-border">SKILLS</span>
      </h3>
      <div id="skill-tree" className="skill-tree"></div>
      <div className="skill-tree-mobile">
        <ul>
          {data.map((skill, index) => (
            <>
              <li
                key={skill.name}
                data-aos="fade-right"
                data-aos-delay={100 * index}
              >
                {skill.name}
              </li>
              <ul>
                {skill.children &&
                  skill.children.map((level, levelIndex) => (
                    <>
                      <li
                        className="level"
                        data-aos="fade-right"
                        data-aos-delay={100 * levelIndex}
                      >
                        {level.name}
                      </li>
                      <ul>
                        {level.children &&
                          level.children.map((lang, langIdex) => (
                            <>
                              <li
                                className="lang"
                                data-aos="fade-right"
                                data-aos-delay={50 * langIdex}
                              >
                                {lang.name}
                              </li>
                              <ul>
                                {lang.children &&
                                  lang.children.map((framework, fwIndex) => (
                                    <>
                                      <li
                                        className="framework"
                                        data-aos="fade-right"
                                        data-aos-delay={50 * fwIndex}
                                      >
                                        {framework.name}
                                      </li>
                                    </>
                                  ))}
                              </ul>
                            </>
                          ))}
                      </ul>
                    </>
                  ))}
              </ul>
            </>
          ))}
        </ul>
      </div>
    </section>
  )
}

export default Skills
