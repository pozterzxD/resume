import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import "./hobby.scss"

interface Props {}

const Hobby = () => {
  const data = useStaticQuery(graphql`
    query {
      photography: file(relativePath: { eq: "camera.png" }) {
        childImageSharp {
          fixed(width: 110) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      gaming: file(relativePath: { eq: "game.png" }) {
        childImageSharp {
          fixed(width: 170) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      coding: file(relativePath: { eq: "computer.png" }) {
        childImageSharp {
          fixed(width: 155) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      collector: file(relativePath: { eq: "shoe.png" }) {
        childImageSharp {
          fixed(width: 180) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return (
    <section id="hobby" data-aos="fade-right">
      <h3>
        <span>HOBBY</span>
      </h3>
      <div className="hobbies">
        <div className="hobby">
          <Img
            fadeIn
            fixed={data.photography.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div data-aos="fade-right" data-aos-delay="200">
            Photography
          </div>
        </div>
        <div className="hobby">
          <Img
            fadeIn
            fixed={data.gaming.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div data-aos="fade-right" data-aos-delay="300">
            Gaming
          </div>
        </div>
        <div className="hobby">
          <Img
            fadeIn
            fixed={data.coding.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div data-aos="fade-right" data-aos-delay="400">
            Coding
          </div>
        </div>
        <div className="hobby">
          <Img
            fadeIn
            fixed={data.collector.childImageSharp.fixed}
            style={{ margin: "0 auto", overflow: "visible" }}
          />
          <div data-aos="fade-right" data-aos-delay="500">
            Collecting
          </div>
        </div>
      </div>
    </section>
  )
}

export default Hobby
