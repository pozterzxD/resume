import React, { useState } from "react"
import "./NavBar.scss"
import "../../utils/fontAwesome"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import scrollTo from "gatsby-plugin-smoothscroll"

const NavBar = () => {
  const [collapsed, SetCollapsed] = useState(false)

  return (
    <>
      <span
        onClick={() => SetCollapsed(!collapsed)}
        className={`navbar-toggle ${collapsed ? "open" : ""}`}
        id="js-navbar-toggle"
      >
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </span>
      <nav className={`nav ${collapsed ? "show" : ""}`}>
        <ul className="navbar">
          <li
            className="nav-item"
            onClick={() => {
              scrollTo("#profile"), SetCollapsed(false)
            }}
            data-aos="fade-left"
          >
            <a>Profile</a>
          </li>
          <li
            className="nav-item"
            onClick={() => {
              scrollTo("#skills"), SetCollapsed(false)
            }}
            data-aos="fade-left"
            data-aos-delay="100"
          >
            <a>Skills</a>
          </li>
          <li
            className="nav-item"
            onClick={() => {
              scrollTo("#works"), SetCollapsed(false)
            }}
            data-aos="fade-left"
            data-aos-delay="200"
          >
            <a>Works</a>
          </li>
          <li
            className="nav-item"
            onClick={() => {
              scrollTo("#contact"), SetCollapsed(false)
            }}
            data-aos="fade-left"
            data-aos-delay="300"
          >
            <a>Contact</a>
          </li>
        </ul>
        <div className="nav-contact">
          <FontAwesomeIcon icon={["fab", "facebook"]} />
        </div>
      </nav>
    </>
  )
}

export default NavBar
