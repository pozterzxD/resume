import React, { ReactElement } from "react"
import "./profile.scss"
import { differenceInYears } from "date-fns"

interface Props {
  dob: Date
}

const getAge = dob => {
  return differenceInYears(new Date(), new Date(dob))
}

function Profile({ dob = new Date() }: Props): ReactElement {
  return (
    <section id="profile" data-aos="fade-up">
      <h3>
        <span className="half-border">PROFILE</span>
      </h3>
      <div className="info">
        <div className="info-left" data-aos="fade-right" data-aos-delay="200">
          Hello, I'm Tharathep,
          <div>
            Mid-level Fullstack Developer & Technical Engineer based in Bangkok
            ― I develop products with knowledge and passion.
          </div>
        </div>
        <div className="info-right">
          <div className="mt-1" data-aos="fade-left" data-aos-delay="400">
            I'm a {getAge(dob)}-year-old man who has Graduated with a Bachelor
            Degree in Department of Computer Engineering at the Prince of
            Songkla University. Phuket Campus, since 2017.
          </div>
          <div className="mt-2" data-aos="fade-left" data-aos-delay="500">
            My career objective is to be able to work for an encouraging company
            that will assist me in developing, improving, and obtaining the
            necessary skills in order to become the best programmer possible.
          </div>
        </div>
      </div>
    </section>
  )
}

export default Profile
