import * as React from "react"
import { useState, useEffect } from "react"
import Layout from "../components/layout/layout"
import Mascot from "../components/mascot/Mascot"
import SEO from "../components/seo"
import Profile from "../components/profile/profile"
import Hobby from "../components/hobby/hobby"
import Skills from "../components/skills/skills"
import Works from "../components/works/works"
import "./index.scss"
import "aos/dist/aos.css"

const dob = new Date("1994-02-02")

const App = () => {
  let AOS
  const [sticky, setSticky] = useState(false)

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", () => {
        if (window.pageYOffset >= 200) {
          setSticky(true)
        } else {
          setSticky(false)
        }
      })
      const AOS = require("aos")
      AOS.init({
        duration: 500,
      })
    }
  }, [])

  useEffect(() => {
    if (AOS) {
      AOS.refresh()
    }
  })

  return (
    <>
      <SEO title="RESUME" />
      <Layout sticky={sticky}>
        <Mascot />
        <Profile dob={dob} />
        <Hobby />
        <Skills />
        <Works />
      </Layout>
    </>
  )
}

export default App
